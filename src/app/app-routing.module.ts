import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ImportNyeisComponent } from './import-nyeis/import-nyeis.component';
import { SuggestedComponent } from './dashboard/suggested/suggested.component';
import { CaseComponent } from './case/case.component';
import { PreviewComponent } from './case/preview/preview.component';


const routes: Routes = [
  { path: '', redirectTo: 'suggested', pathMatch: 'full'},
  { path: 'suggested', component: SuggestedComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'importnyeis', component: ImportNyeisComponent },
  { path: 'case', component: CaseComponent },
  { path: 'preview', component: PreviewComponent },
  ];
  
  @NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
  })
  export class AppRoutingModule { }