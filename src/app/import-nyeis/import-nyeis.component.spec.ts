import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportNyeisComponent } from './import-nyeis.component';

describe('ImportNyeisComponent', () => {
  let component: ImportNyeisComponent;
  let fixture: ComponentFixture<ImportNyeisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportNyeisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportNyeisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
