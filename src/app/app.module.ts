import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DashboardComponent } from './dashboard/dashboard.component';

import { CaseComponent } from './case/case.component';
import { PreviewComponent } from './case/preview/preview.component';
import { CaseSidebarComponent } from './case/sidebar/sidebar.component';
import { ListComponent } from './case/list/list.component';
import { NoteComponent } from './case/note/note.component';
import { SuggestedComponent } from './dashboard/suggested/suggested.component';
import { FilterComponent } from './dashboard/filter/filter.component';
import { SidebarComponent } from './import-nyeis/sidebar/sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CaseComponent,
    PreviewComponent,
    CaseSidebarComponent,
    ListComponent,
    NoteComponent,
    SuggestedComponent,
    FilterComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
