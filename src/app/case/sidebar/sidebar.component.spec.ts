import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseSidebarComponent } from './sidebar.component';

describe('CaseSidebarComponent', () => {
  let component: CaseSidebarComponent;
  let fixture: ComponentFixture<CaseSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
